# Smartfarming

@Author : Amanda Hayoz

@School : HES-SO, Switzerland

@Institute : iSIS, HEIA-FR, Fribourg, Switzerland

@Description : Semester project done during second semester of the master MES in computer sciences (orientation : embedded and mobile systems). 

---------------------------------------------------------------------------------------------------
This repository contains the documentation created during the project. For source code, refer to the following repository : https://gitlab.forge.hefr.ch/heiafr-smartfarming/stm32mp15-ecosystem-v2.1.0 .

---------------------------------------------------------------------------------------------------
